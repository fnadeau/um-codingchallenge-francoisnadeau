//
//  MovieCollectionViewController.h
//  SampleApp
//
//  Created by フランソア on 2013-08-23.
//  Copyright (c) 2013 Francois Nadeau. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieCollectionViewController : UICollectionViewController

@property (strong, nonatomic) IBOutlet UICollectionView *collection;

@end
