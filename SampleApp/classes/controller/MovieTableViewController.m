//
//  MovieTableViewController.m
//  SampleApp
//
//  Created by フランソア on 2013-08-23.
//  Copyright (c) 2013 Francois Nadeau. All rights reserved.
//
#import "MovieTableViewController.h"
#import "MovieTableCell.h"
#import "DataManager.h"
#import "Movie.h"

@implementation MovieTableViewController

-(void)awakeFromNib {
    [super awakeFromNib];
    
    [[DataManager sharedInstance] addObserver:self
                                   forKeyPath:@"movies"
                                      options:NSKeyValueObservingOptionNew
                                      context:nil];
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([@"movies" isEqualToString:keyPath]) {
        [self.tableView reloadData];
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

-(void)dealloc {
    [[DataManager sharedInstance] removeObserver:self forKeyPath:@"movies"];
}

#pragma mark - Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Top 25 box office movies";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [DataManager sharedInstance].movies.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"movieTableCell";
    MovieTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    Movie *theMovie = [[DataManager sharedInstance].movies objectAtIndex:indexPath.row];
    cell.movie = theMovie;
    cell.titleLabel.text = theMovie.title;

    // Retrieve the Theater release date
    NSDictionary *releaseDates = [NSKeyedUnarchiver unarchiveObjectWithData:theMovie.releaseDate];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    NSDate *theTheaterDate = [format dateFromString:[releaseDates objectForKey:@"theater"]];

    // Display the year for this release.
    [format setDateFormat:@"yyyy"];
    cell.yearLabel.text = [format stringFromDate:theTheaterDate];
    cell.criticScoreLabel.text = [NSString stringWithFormat:@"%@%%", theMovie.criticScore];
    cell.imageView.image = [UIImage imageWithData:theMovie.thumbnailImage];
    
    return cell;
}

@end
