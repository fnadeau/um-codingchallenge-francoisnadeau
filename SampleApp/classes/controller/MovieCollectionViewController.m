//
//  MovieCollectionViewController.m
//  SampleApp
//
//  Created by フランソア on 2013-08-23.
//  Copyright (c) 2013 Francois Nadeau. All rights reserved.
//
#import "MovieCollectionViewController.h"
#import "MovieCollectionViewCell.h"
#import "DataManager.h"
#import "Movie.h"

@implementation MovieCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)awakeFromNib {
    [[DataManager sharedInstance] addObserver:self
                                   forKeyPath:@"movies"
                                      options:NSKeyValueObservingOptionNew
                                      context:nil];

    
    [super awakeFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)dealloc {
    [[DataManager sharedInstance] removeObserver:self forKeyPath:@"movies"];
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([@"movies" isEqualToString:keyPath]) {       
        [self.collection reloadData];
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [DataManager sharedInstance].movies.count;    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MovieCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"movieColletionCell" forIndexPath:indexPath];
    
    Movie *theMovie = [[DataManager sharedInstance].movies objectAtIndex:indexPath.row];
    cell.movie = theMovie;
    cell.imageView.image = [UIImage imageWithData:theMovie.thumbnailImage];
    cell.titleLabel.text = theMovie.title;
    
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

@end
