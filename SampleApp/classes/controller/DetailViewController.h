//
//  DetailViewController.h
//  SampleApp
//
//  Created by フランソア on 2013-08-25.
//  Copyright (c) 2013 Francois Nadeau. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Movie;

@interface DetailViewController : UIViewController

@property (nonatomic, weak) Movie *movie;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
