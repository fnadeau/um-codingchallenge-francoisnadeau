//
//  MovieCollectionViewCell.h
//  SampleApp
//
//  Created by フランソア on 2013-08-23.
//  Copyright (c) 2013 Francois Nadeau. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Movie;

@interface MovieCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) Movie *movie;
@end
