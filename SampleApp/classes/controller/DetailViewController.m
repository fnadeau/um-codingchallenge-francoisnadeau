//
//  DetailViewController.m
//  SampleApp
//
//  Created by フランソア on 2013-08-25.
//  Copyright (c) 2013 Francois Nadeau. All rights reserved.
//

#import "DetailViewController.h"
#import "DataManager.h"
#import "Movie.h"
#import "Actor.h"

@implementation DetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    self.title = self.movie.title;

    NSMutableString *theHtml = [[NSMutableString alloc] init];
    [theHtml appendFormat:@"<center><img width=300 src=\"file://%@\"/></center>", [[DataManager sharedInstance] posterImagePathFor:self.movie]];

    [theHtml appendFormat:@"<P><B>MPAA rating :</B> %@", self.movie.rating];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    [theHtml appendString:@"<P><B>Release dates :</B><UL>"];
    
    NSDictionary *releaseDates = [NSKeyedUnarchiver unarchiveObjectWithData:self.movie.releaseDate];
    for(NSString *oneKey in releaseDates) {
        [theHtml appendFormat:@"<LI><B>%@</B> - %@", oneKey, [releaseDates objectForKey:oneKey]];
    }
    [theHtml appendString:@"</UL>"];
    
    [theHtml appendFormat:@"<P><B>Critic consensus :</B> %@", self.movie.criticConsensus];
    [theHtml appendFormat:@"<P><B>Critic Score :</B> %@%%", self.movie.criticScore];
    [theHtml appendFormat:@"<P><B>Audience Score :</B> %@%%", self.movie.audianceScore];
    [theHtml appendFormat:@"<P><B>Synopsis :</B> %@", self.movie.synopsis];
    
    [theHtml appendString:@"<P><B>Cast :</B> <UL>"];
    for(Actor *oneActor in self.movie.casts) {
        [theHtml appendFormat:@"<LI>%@", oneActor.actorName];
    }
    [theHtml appendString:@"</UL>"];
    
    [self.webView loadHTMLString:theHtml
                         baseURL:[NSURL fileURLWithPath:@"/"]];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

@end
