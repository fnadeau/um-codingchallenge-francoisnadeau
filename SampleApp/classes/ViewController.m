//
//  ViewController.m
//  SampleApp
//
//  Created by フランソア on 2013-08-23.
//  Copyright (c) 2013 Francois Nadeau. All rights reserved.
//
#import "ViewController.h"
#import "DetailViewController.h"
#import "MovieTableViewController.h"
#import "MovieTableCell.h"
#import "DataManager.h"

@interface ViewController ()
@end

@implementation ViewController

- (void)viewDidLoad {
    self.collectionViewController.view.hidden = YES;
    [self checkForMovies];
    [[DataManager sharedInstance] addObserver:self
                                   forKeyPath:@"movies"
                                      options:NSKeyValueObservingOptionNew
                                      context:nil];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)checkForMovies {
    if([DataManager sharedInstance].movies.count == 0) {
        [self.indicator startAnimating];
    } else {
        [self.indicator stopAnimating];
    }
}

-(void)dealloc {
    [[DataManager sharedInstance] removeObserver:self forKeyPath:@"movies"];
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([@"movies" isEqualToString:keyPath]) {
        [self checkForMovies];
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (IBAction)listSelectionTouchUp:(UISegmentedControl *)sender {
    switch (sender.selectedSegmentIndex) {
        case 0:
            self.tableViewController.view.hidden = NO;
            self.collectionViewController.view.hidden = YES;
            break;
        case 1:
            self.tableViewController.view.hidden = YES;
            self.collectionViewController.view.hidden = NO;
            break;
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([sender respondsToSelector:@selector(movie)]){
        DetailViewController *controller = segue.destinationViewController;
        controller.movie = [sender performSelector:@selector(movie)];
    } else {
        // Don't know how to handle this segue...
    }
}

@end
