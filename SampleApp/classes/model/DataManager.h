//
//  DataManager.h
//  SampleApp
//
//  Created by フランソア on 2013-08-25.
//  Copyright (c) 2013 Francois Nadeau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Movie;

@interface DataManager : NSObject

@property (nonatomic, strong, readonly) NSArray *movies;

@property (nonatomic, strong, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

-(Movie*)getMovieForId:(NSString*)movieId;
-(NSString*)posterImagePathFor:(Movie*)movie;

+(DataManager*)sharedInstance;

@end