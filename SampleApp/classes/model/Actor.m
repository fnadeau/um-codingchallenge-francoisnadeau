//
//  Actor.m
//  SampleApp
//
//  Created by フランソア on 2013-08-25.
//  Copyright (c) 2013 Francois Nadeau. All rights reserved.
//

#import "Actor.h"


@implementation Actor

@dynamic actorName;
@dynamic actorId;
@dynamic movies;

@end
