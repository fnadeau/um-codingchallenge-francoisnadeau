//
//  Actor.h
//  SampleApp
//
//  Created by フランソア on 2013-08-25.
//  Copyright (c) 2013 Francois Nadeau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Actor : NSManagedObject

@property (nonatomic, retain) NSString * actorName;
@property (nonatomic, retain) NSString * actorId;
@property (nonatomic, retain) NSSet *movies;
@end

@interface Actor (CoreDataGeneratedAccessors)

- (void)addMoviesObject:(NSManagedObject *)value;
- (void)removeMoviesObject:(NSManagedObject *)value;
- (void)addMovies:(NSSet *)values;
- (void)removeMovies:(NSSet *)values;

@end
