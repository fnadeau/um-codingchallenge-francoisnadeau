//
//  Movie.h
//  SampleApp
//
//  Created by フランソア on 2013-08-27.
//  Copyright (c) 2013 Francois Nadeau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Actor;

@interface Movie : NSManagedObject

@property (nonatomic, retain) NSNumber * audianceScore;
@property (nonatomic, retain) NSString * criticConsensus;
@property (nonatomic, retain) NSNumber * criticScore;
@property (nonatomic, retain) NSString * movieId;
@property (nonatomic, retain) NSNumber * order;
@property (nonatomic, retain) NSString * rating;
@property (nonatomic, retain) NSData * releaseDate;
@property (nonatomic, retain) NSString * synopsis;
@property (nonatomic, retain) NSData * thumbnailImage;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSSet *casts;
@end

@interface Movie (CoreDataGeneratedAccessors)

- (void)addCastsObject:(Actor *)value;
- (void)removeCastsObject:(Actor *)value;
- (void)addCasts:(NSSet *)values;
- (void)removeCasts:(NSSet *)values;

@end
