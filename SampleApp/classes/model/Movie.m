//
//  Movie.m
//  SampleApp
//
//  Created by フランソア on 2013-08-27.
//  Copyright (c) 2013 Francois Nadeau. All rights reserved.
//

#import "Movie.h"
#import "Actor.h"


@implementation Movie

@dynamic audianceScore;
@dynamic criticConsensus;
@dynamic criticScore;
@dynamic movieId;
@dynamic order;
@dynamic rating;
@dynamic releaseDate;
@dynamic synopsis;
@dynamic thumbnailImage;
@dynamic title;
@dynamic casts;

@end
