//
//  DataManager.m
//  SampleApp
//
//  Created by フランソア on 2013-08-25.
//  Copyright (c) 2013 Francois Nadeau. All rights reserved.
//

#import "DataManager.h"
#import "AFJSONRequestOperation.h"
#import "AFImageRequestOperation.h"
#import "Movie.h"
#import "Actor.h"

@interface DataManager()
@property (nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) NSURL *storeURL;
@property (nonatomic, strong) NSArray *movies;
@end

@implementation DataManager

-(id)_init {
    self = [super init];
    if(self) {
        [self setupCoreData];
        [self fetchAllMovies];
        [self updateData];
    }
    
    return self;
}

-(void)setupCoreData {
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"DataModel" withExtension:@"momd"];
    self.managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    self.managedObjectContext = [[NSManagedObjectContext alloc] init];
    
    NSURL *directoryURL = [[[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject];
    self.storeURL = [directoryURL URLByAppendingPathComponent:@"model.sqlite"];
    
    self.persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
    
    NSError *error = nil;
    if ([self.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                      configuration:nil
                                                                URL:self.storeURL
                                                            options:nil
                                                              error:&error])  {
        // All is well...
    } else {
        // We have an error...
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops!"
                                                        message:@"Something has gone terribly wrong! You need to reinstall the app in order for it to work properly."
                                                       delegate:nil
                                              cancelButtonTitle:@"Close."
                                              otherButtonTitles:nil, nil];
        [alert show];
        
#warning TODO : Come up with a nicer error. For exmample we could errase the offending data file and recreate a new one to save the user from having to errase the app....
        
    }
    
    [self.managedObjectContext setPersistentStoreCoordinator:self.persistentStoreCoordinator];
    
    // Track data modifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDataModelChange:)
                                                 name:NSManagedObjectContextObjectsDidChangeNotification
                                               object:self.managedObjectContext];
}

-(void)fetchAllMovies {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Movie"
                                        inManagedObjectContext:self.managedObjectContext]];
    [fetchRequest setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"order" ascending:NO]]];
    
    NSError *error;
    self.movies = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
}

- (void)handleDataModelChange:(NSNotification *)note {
    [self fetchAllMovies];
}

-(void)updateData {
    NSURL *url = [NSURL URLWithString:TOP_GROSSING_MOVIES];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        //NSLog(@"Stream: %@", JSON);
        NSDictionary *theRootDictionary = JSON;
        NSArray *theMovieArray = [theRootDictionary objectForKey:@"movies"];
        
        int theMovieRanking = 0;
        for(NSDictionary *oneMovie in theMovieArray) {
            // Check if the movie is already in the database...
            Movie *theMovie = [self getMovieForId:[oneMovie objectForKey:@"id"]];
            if(theMovie == nil) {
                // This is a new movie, so lets create it
                theMovie = [NSEntityDescription insertNewObjectForEntityForName:@"Movie"
                                                         inManagedObjectContext:self.managedObjectContext];
                [self updateMovieFields:theMovie
                             dictionary:oneMovie];
                theMovie.order = [NSNumber numberWithInt:theMovieRanking];
                theMovieRanking++;
                
                NSError *error;
                if ([self.managedObjectContext save:&error]) {
                    // The movie was properly saved.
                } else {
                    NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                }
                
            } else {
                // This movie is already in the database, so lets update it
#warning TODO need to update movies.
            }
        }

        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
#warning TODO : Need to present a nicer error if we can't reach the site...
        NSLog(@"Error: %@", error);
    }];
    [operation start];
}

-(NSString*)posterImagePathFor:(Movie*)movie {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString * path = [[NSString alloc] initWithString:[documentsDirectory stringByAppendingPathComponent:@"images"]];
    [[NSFileManager defaultManager] createDirectoryAtPath:path
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
    return [NSString stringWithFormat:@"%@.jpg",[path stringByAppendingPathComponent:movie.movieId]];
}

-(void)updateMovieFields:(Movie*)movie dictionary:(NSDictionary*)dictionary {   
    movie.movieId = [dictionary objectForKey:@"id"];
    movie.title = [dictionary objectForKey:@"title"];
    
    // Save the NSDictionary of release dates as is...
    movie.releaseDate = [NSKeyedArchiver archivedDataWithRootObject:[dictionary objectForKey:@"release_dates"]];

    movie.criticScore = [[dictionary objectForKey:@"ratings"] objectForKey:@"critics_score"];
    movie.audianceScore = [[dictionary objectForKey:@"ratings"] objectForKey:@"audience_score"];
    movie.rating = [dictionary objectForKey:@"mpaa_rating"];
    movie.criticConsensus = [dictionary objectForKey:@"critics_consensus"];
    movie.synopsis = [dictionary objectForKey:@"synopsis"];

    NSString *thumbnailURL = [[dictionary objectForKey:@"posters"] objectForKey:@"thumbnail"];
    NSString *posterURL = [[dictionary objectForKey:@"posters"] objectForKey:@"original"];
    
    movie.thumbnailImage = [NSData dataWithContentsOfURL:[NSURL URLWithString:thumbnailURL]];
    
    // Save the poster image to the local disk.
    NSData *posterImage = [NSData dataWithContentsOfURL:[NSURL URLWithString:posterURL]];
    [posterImage writeToFile:[self posterImagePathFor:movie]
                  atomically:NO];
    
    for(NSDictionary *oneActor in [dictionary objectForKey:@"abridged_cast"]) {
        [movie addCastsObject:[self getActorForDictionary:oneActor]];
    }
}

-(Actor*)getActorForDictionary:(NSDictionary*)actorDictionary {
    NSString *actorId = [actorDictionary objectForKey:@"id"];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Actor"
                                              inManagedObjectContext:self.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"actorId == %@", actorId];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *theActors = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if(theActors.count == 0) {
        // Actor does not exist yet, lets create it in the database.
        Actor *theActor = [NSEntityDescription insertNewObjectForEntityForName:@"Actor"
                                                        inManagedObjectContext:self.managedObjectContext];
        theActor.actorName = [actorDictionary objectForKey:@"name"];
        theActor.actorId = actorId;
        
        return theActor;
    } else {
        if(theActors.count > 1) {
            NSLog(@"Very strange situation since we have multiple actors with the same id == %@", actorId);
        } else {
            // This is what we expect.
        }
        
        return [theActors objectAtIndex:0];
    }

}

-(Movie*)getMovieForId:(NSString*)movieId {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Movie"
                                              inManagedObjectContext:self.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"movieId == %@", movieId];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];

    NSError *error;
    NSArray *theMovies = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if(theMovies.count == 0) {
        return nil;
    } else {
        if(theMovies.count > 1) {
            NSLog(@"Very strange situation since we have multiple movies with the same id == %@", movieId);
        } else {
            // This is what we expect.
        }
        
        return [theMovies objectAtIndex:0];
    }
}

+(DataManager*)sharedInstance {
    static DataManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[DataManager alloc] _init];
    });
    
    return instance;
}

@end