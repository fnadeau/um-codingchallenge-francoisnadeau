//
//  ViewController.h
//  SampleApp
//
//  Created by フランソア on 2013-08-23.
//  Copyright (c) 2013 Francois Nadeau. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MovieTableViewController;

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet MovieTableViewController *tableViewController;
@property (strong, nonatomic) IBOutlet UICollectionViewController *collectionViewController;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end