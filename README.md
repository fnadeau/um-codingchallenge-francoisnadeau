# Universal Mind iPhone Sample Application Specification

Welcome to the Universal Mind Code Sample specification! We are thrilled that you are interested in joining our elite team of technologists.

In order to proceed with the interview process, we'd like to see a code sample from you. This document describes a simple iPhone application that we'd like you to build and then submit to us as that code sample.

We expect that this sample application can be completed in a reasonable amount of time, around 8 hours. However, we would rather have a high quality sample that is incomplete than a low quality sample that includes all of the features.

# Instructions

Using BitBucket, create a *private* fork of this repository. We ask for a private fork to keep others from seeing the code sample that you will be creating for us.

In your private repository, build an iPhone application that conforms to the specifications below. Commit and push as you normally would.

Once your application is finished, grant the "universalmind" BitBucket user read access to your repository that contains code for the working sample application. We will then arrange an interview time that fits all of our schedules to review the application with you.

That's it!

Are you ready? Here's what we would like to see as your code sample...

# App Requirements

Use the Rotten Tomatoes JSON API... 

* [http://developer.rottentomatoes.com/docs](http://developer.rottentomatoes.com/docs)
* [http://developer.rottentomatoes.com/iodocs](http://developer.rottentomatoes.com/iodocs)

... to create an iPhone application that displays relevant information to the user.

The application needs to have the following required functionality:

1. Connect to the Rotten Tomatoes REST API to retrieve the top 25 box office earning movies.
2. Present the list of movies in a table view. Create a custom UITableViewCell for displaying the movie information. The cell should display the movie thumbnail, the movie title, the movie year, and the critic score as a percentage.
3. Present the list of movies in a collection view. The collection view should scroll movies horizontally. Each cell should contain the movie thumbnail with the movie title underneath it.
4. Use a segmented control in the Navigation Bar to switch between the table view and collection view.
5. Tapping a movie (in either view) should use a navigation controller to push a new view that displays detailed information about the movie. Include the poster image, the MPAA rating, release dates, critic consensus, critic and audience scores, synopsis, and list of cast members.

If you are pressed for time, you may treat steps 3 and 4 as optional. However, be prepared to talk about them anyway during review.

Please keep the following constraints in mind:

1. Use ARC.
2. Use Core Data.
3. Use Storyboards.
4. You are free, and encouraged, to use open-source libraries. Any open-source libraries that you use must be linked as git submodules in your code base. 

## Bonus Points

1. Filtering: Allow the data to be filtered by a movie title that the user can type into a text input.
2. Sorting: Allow the data to be sorted by movie title ascending or descending.
3. Favorites: Allow the user to mark the movie as a favorite or not. Allow the user to filter movies by their favorites.
4. Design: Show us that you know how to cut up design files. Skin your application to make it stand out from the crowd.
5. Git: Use git-flow during development to track features.
